package com.martinartime.solstice;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;
import com.martinartime.solstice.retrofit.APIService;
import com.martinartime.solstice.retrofit.RetrofitClient;

import io.reactivex.disposables.CompositeDisposable;


public class App extends Application {

    public static Gson gson;
    public static APIService apiService;
    public static CompositeDisposable compositeDisposable;

    private static App instance;

    public static App getInstance() {
        return instance;
    }

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    public static APIService getAPIService() {
        return apiService;
    }

    public static Gson getGson() {
        return gson;
    }

    public static CompositeDisposable getCompositeDisposable() {
        return compositeDisposable;
    }

    @Override
    public void onCreate() {
        instance = this;
        super.onCreate();

        gson = new Gson();
        compositeDisposable = new CompositeDisposable();
        apiService = RetrofitClient.getClient(getResources().getString(R.string.base_url)).create(APIService.class);
    }

}

