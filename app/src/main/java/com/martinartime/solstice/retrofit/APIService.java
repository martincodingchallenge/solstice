package com.martinartime.solstice.retrofit;

import com.martinartime.solsticemodel.Contact;

import java.util.List;

import io.reactivex.Flowable;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Interface that contains the signature methods with all the posisble requests to the server
 *
 *  Creado por MartinArtime
 */
public interface APIService {

    /**
     * Query all contacts
     */
    @GET("technical-challenge/v3/contacts.json")
    Flowable<List<Contact>> searchContacts();

}

